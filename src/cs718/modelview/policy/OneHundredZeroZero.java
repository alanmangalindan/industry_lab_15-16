package cs718.modelview.policy;

import cs718.modelview.model.StudentResult;
import cs718.modelview.model.Percentage;

/**
 * A course assessment policy. With this policy, a course is assessed
 * exclusively by exam.
 * 
 */
public class OneHundredZeroZero implements AssessmentPolicy {

	public Percentage calculate(StudentResult result) {
		return result
				.getAssessmentElement(StudentResult.AssessmentElement.Exam);
	}

	public String toString() {
		return "Exam only";
	}

}
