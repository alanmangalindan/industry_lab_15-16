package cs718.modelview.gui;

import cs718.modelview.model.Course;
import cs718.modelview.model.CourseListener;

public class DistributionPanelAdapter implements CourseListener {

	/**********************************************************************
	 * YOUR CODE HERE
	 */

	private DistributionPanel distPanel;

	public DistributionPanelAdapter(DistributionPanel distPanel) {
		this.distPanel = distPanel;
	}


	@Override
	public void courseHasChanged(Course course) {
		distPanel.repaint();
	}
}
