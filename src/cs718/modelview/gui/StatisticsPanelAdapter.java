package cs718.modelview.gui;

import cs718.modelview.model.Course;
import cs718.modelview.model.CourseListener;

public class StatisticsPanelAdapter implements CourseListener {
	
	/**********************************************************************
	 * YOUR CODE HERE
	 */
	private StatisticsPanel statPanel;

	public StatisticsPanelAdapter(StatisticsPanel statPanel) {
		this.statPanel = statPanel;
	}

	@Override
	public void courseHasChanged(Course course) {
		statPanel.repaint();
	}
}
