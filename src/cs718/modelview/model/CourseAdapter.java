package cs718.modelview.model;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

public class CourseAdapter extends AbstractTableModel implements CourseListener {
	
	/**********************************************************************
	 * YOUR CODE HERE
	 */

	private Course thisCourse; // thisCourse is the adaptee
	private JTable tableView;

	String[] columnNames = {"Student ID",
			"Surname",
			"Forename",
			"Exam",
			"Test",
			"Assignment",
			"Overall"};

	public CourseAdapter(Course thisCourse) {
		this.thisCourse = thisCourse;
//		createTable();
	}

//	public void createTable() {
//		for (int i = 0; i < thisCourse.size(); i++) {
//			System.out.println(thisCourse.getResultAt(i));
//		}
//	}

	@Override
	public int getRowCount() {
		return thisCourse.size();
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public String getColumnName(int columnIndex) {
		return columnNames[columnIndex];
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		switch (columnIndex) {
			case 0:
				return Integer.class;
			case 1:
				return String.class;
			case 2:
				return String.class;

				default:
					return Percentage.class;
		}
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		StudentResult result = thisCourse.getResultAt(rowIndex);

		switch (columnIndex) {
			case 0:
				return result._studentID;
			case 1:
				return result._studentSurname;
			case 2:
				return result._studentForename;
			case 3:
				return result.getAssessmentElement(StudentResult.AssessmentElement.Exam);
			case 4:
				return result.getAssessmentElement(StudentResult.AssessmentElement.Test);
			case 5:
				return result.getAssessmentElement(StudentResult.AssessmentElement.Assignment);
			case 6:
				return result.getAssessmentElement(StudentResult.AssessmentElement.Overall);
		}

		return null;
	}


	@Override
	public void courseHasChanged(Course course) {
//		System.out.println("Pointing to same course Object?: " + Boolean.toString(thisCourse == course));
		fireTableDataChanged();
	}
}