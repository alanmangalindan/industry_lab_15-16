package cs718.mytestpackage;

public class MyTestClass {

    // HOME_KEY is a key that identifies the user's home directory.
    private static final String HOME_KEY = "user.home";

    public void makeFileStore() {
        // Get user's home directory path.
        String home = System.getProperty( HOME_KEY );
        java.io.File root = new java.io.File(home);

        ClassWithinAClass cwac = new ClassWithinAClass();

    }

    class ClassWithinAClass {

        ClassWithinAClass() {
            System.out.println("I am a Class within a Class!");
        }
    }

}

class ClassWithinJavaFile {

    ClassWithinJavaFile() {
        System.out.println("I am a another Class in the same Java file!");
    }
}
