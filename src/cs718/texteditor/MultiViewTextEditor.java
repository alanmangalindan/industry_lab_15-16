package cs718.texteditor;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;

public class MultiViewTextEditor extends JPanel {
	
	private Document _model;
	private JTextArea _view1;
	private JTextArea _view2;
	private JLabel _statusInfo;
	
	public MultiViewTextEditor() {
		_model = new PlainDocument();
		_view1 = new JTextArea();
		_view2 = new JTextArea();
		_statusInfo = new JLabel(" ");
		
		_view1.setDocument(_model);
		_view2.setDocument(_model);
		layoutGUI();
	}
	
	private void layoutGUI() {
		JPanel viewsPane = new JPanel();
		viewsPane.setLayout(new GridLayout(2,1));
		
		JScrollPane scrollPaneForView1 = new JScrollPane();
		scrollPaneForView1.setViewportView(_view1);
		
		JScrollPane scrollPaneForView2 = new JScrollPane();
		scrollPaneForView2.setViewportView(_view2);
		 
		viewsPane.add(scrollPaneForView1);
		viewsPane.add(scrollPaneForView2);
		
		JMenuBar menuBar = createMenuBar();
		
		setLayout(new BorderLayout());
		add(menuBar, BorderLayout.NORTH);
		add(_statusInfo, BorderLayout.SOUTH);
		add(viewsPane, BorderLayout.CENTER);
		
		setPreferredSize(new Dimension(600,800));
	}
	
	private JMenuBar createMenuBar() {
		JMenuBar menuBar = new JMenuBar();
		
	    JMenu file = new JMenu("File");

	    JMenuItem item = new JMenuItem(new NewAction());
	    file.add(item);

	    file.add(item = new JMenuItem(new OpenAction()));

	    file.addSeparator();
	    file.add(item = new JMenuItem(new ExitAction()));
	    menuBar.add(file);

	    return menuBar;
	  }

		  
    private static void createAndShowGUI() {
       // Create and set up the window.
    	JFrame frame = new JFrame("MultiViewTextEditor");
       frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

       // Create and set up the content pane.
       JComponent newContentPane = new MultiViewTextEditor();
       frame.add(newContentPane);
       
       // Display the window.
       frame.pack();
       frame.setLocationRelativeTo(null);
       frame.setVisible(true);
   }

   public static void main(String[ ] args) {
       // Schedule a job for the event-dispatching thread:
       // creating and showing this application's GUI.
       javax.swing.SwingUtilities.invokeLater(new Runnable() {
           public void run() {
               createAndShowGUI();
           }
       });
   }
   
   
   class NewAction extends AbstractAction {
	   public NewAction() {
		   putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.ALT_MASK));
		   putValue(Action.NAME, "New");
		   putValue(Action.SHORT_DESCRIPTION, "Start over with a new document");
	   }
	   
		@Override
		public void actionPerformed(ActionEvent event) {
			try {
				_model.remove(0, _model.getLength());
			} catch(BadLocationException e) {
				// Ignore.
			}
		}
	}
   
   class OpenAction extends AbstractAction {
	   public OpenAction() {
		   putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.ALT_MASK));
		   putValue(Action.NAME, "Open");
		   putValue(Action.SHORT_DESCRIPTION, "Open a file");
	   }
	   
	   @Override
	   public void actionPerformed(ActionEvent e) {
		   JFileChooser fileChooser = new JFileChooser();
			 int returnVal = fileChooser.showOpenDialog(MultiViewTextEditor.this);
			 
			 if(returnVal == JFileChooser.APPROVE_OPTION) {
				 File file = fileChooser.getSelectedFile();
				 
			      setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			      try {
			        // Read in text file
			        FileReader fin = new FileReader(file);
			        BufferedReader br = new BufferedReader(fin);
			        char buffer[] = new char[4096];
			        int len;
			        while ((len = br.read(buffer, 0, buffer.length)) != -1) {
			          // Insert into pane
			          _model.insertString(_model.getLength(), new String(buffer, 0, len), null);
			        }
			        
			        br.close();
			      } catch (BadLocationException exc) {
			        _statusInfo.setText("Error loading: " + file.getName());
			      } catch (FileNotFoundException exc) {
			        _statusInfo.setText("File Not Found: " + file.getName());
			      } catch (IOException exc) {
			        _statusInfo.setText("IOException: " + file.getName());
			      }
			      setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
			    }
	   }
   }
   
   class ExitAction extends AbstractAction {
	   public ExitAction() {
		   putValue(Action.NAME, "New");
		   putValue(Action.SHORT_DESCRIPTION, "Exit the application");
	   }
	   
	   @Override
	   public void actionPerformed(ActionEvent e) {
		   System.exit(0);
	   }
   }
}
